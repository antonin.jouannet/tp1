
---

__*TP 1*__

# **I. Self-footprinting**

# **Host OS**

🌞 Déterminer les principales informations de votre machine

**Nom de la machine :** J'utilise la commande _*"hostname"*_ et j'obtient "LAPTOP-Q13R3KDP"

**OS et version:** j'utilise la commande _*"systeminfo"*_ et je récupère les données suivantes [...]

```
Nom du système d’exploitation:              Microsoft Windows 10 Famille
Version du système:                         10.0.18363 N/A version 18363
```

[...]


**Architecture processeur (32-bit, 64-bit, ARM, etc) :** J'utilise la commande _*"systeminfo"*_ et je récupère les données suivantes [...]

```
Type du système:                            x64-based PC

```
[...]


**Quantité RAM et modèle de la RAM :** J'utilise la commande "systeminfo" et je récupère les données suivantes : 
[...]
```Mémoire physique totale:                    7 104 Mo
Mémoire physique disponible:                2 849 Mo
Mémoire virtuelle : taille maximale:        10 816 Mo
Mémoire virtuelle : disponible:             4 911 Mo
Mémoire virtuelle : en cours d’utilisation: 5 905 Mo
```
[...]



---


# **Devices**

🌞 Trouver

**la marque et le modèle de votre processeur :** 

* La marque et le modèle du processeur : commande _*"systeminfo"*_

[...]

```
Processeur(s):                              1 processeur(s) installé(s).
                                            [01] : AMD64 Family 23 Model 24 Stepping 1 AuthenticAMD ~2100 MHz
```
[...]

* Identifier le nombre de processeurs, le nombre de coeur : commande _*" WMIC CPU Get DeviceID,NumberOfCores,NumberOfLogicalProcessors"*_

```
DeviceID  NumberOfCores  NumberOfLogicalProcessors
CPU0      4              8
```

* si c'est un proc Intel, expliquer le nom du processeur



**la marque et le modèle :** 

* de votre touchpad/trackpad

...

* de votre carte graphique : commande _*"wmic path win32_VideoController get name"*_
* 
```
Name
AMD Radeon(TM) Vega 8 Graphics
```

Ici il y a le processeur en plus donc je l'enlève et il reste la carte graphique :

```Vega 8 Graphics```

🌞 Disque dur

**Identifier la marque et le modèle de votre(vos) disque(s) dur(s) :** J'utilise la commande _*"wmic diskdrive get model,name,serialnumber"*_

```
Model                       Name                SerialNumber
SAMSUNG MZVLB256HBHQ-00000  \\.\PHYSICALDRIVE0  0025_3885_01D6_D75B.
```

**Identifier les différentes partitions de votre/vos disque(s) dur(s) :** J'utilise dans un premier temps la commande _*"diskpart"*_ qui m'affiche une nouvelle invite de commande avec indiqué dessus :

```
Sur l’ordinateur : LAPTOP-Q13R3KDP

DISKPART>
```


ensuite j'exécute la commande _*"list disk"*_ qui m'affiche :

```
  N° disque  Statut         Taille   Libre    Dyn  GPT
  ---------  -------------  -------  -------  ---  ---
  Disque 0    En ligne        238 G octets      0 octets
```


puis j'exécute ensuite la commande _*"select disk 0"*_ qui affiche : 

```
Le disque 0 est maintenant le disque sélectionné.
```


Après j'exécute la commande _*"detail disk"*_ qui m'affiche  :

```
SAMSUNG MZVLB256HBHQ-00000
ID du disque : {E843B848-E31A-4073-9922-59D023D855DC}
Type : NVMe
État : En ligne
Chemin : 0
Cible : 0
ID LUN : 0
Chemin d’accès de l’emplacement : PCIROOT(0)#PCI(0103)#PCI(0000)#NVME(P00T00L00)
État en lecture seule actuel : Non
Lecture seule : Non
Disque de démarrage : Oui
Disque de fichiers d’échange : Oui
Disque de fichiers de mise en veille prolongée : Non
Disque de fichiers de vidage sur incident : Oui
Disque en cluster  : Non

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 0     C   Windows      NTFS   Partition     80 G   Sain       Démarrag
  Volume 1     D   Data         NTFS   Partition    142 G   Sain
  Volume 2         SYSTEM       FAT32  Partition    100 M   Sain       Système
```

Enfin la dernière commande qui me permettra de voir les différentes partitions de mon/mes disque(s) dur(s) est la commande _*"list partition"*_

```
  N° partition   Type              Taille   Décalage
  -------------  ----------------  -------  --------
  Partition 1    Système            100 M   1024 K
  Partition 2    Réservé             16 M    101 M
  Partition 3    Principale          80 G    117 M
  Partition 4    Principale         142 G     80 G
  Partition 5    Récupération       512 M    222 G
  Partition 6    Récupération        14 G    223 G
  Partition 7    Récupération      1024 M    237 G
```

**Déterminer le système de fichier de chaque partition :**

J'utilise la commande _*"select disk 0"*_ qui m'affiche :

```
Le disque 0 est maintenant le disque sélectionné.
```

Enfin je sélectionne chaque partition en utilisant _*"select partition"*_ suivi du numéro de celle-ci, puis j'affiche les détails de la partition en exécutant la commande _*"detail partition"*_. J'applique cela sur toute mes partitions qui sont au nombre de 7.

Pour exemple, je l'applique sur la partition 1, les commandes mises en exemples seront les mêmes pour toutes ces dernières en changeant bien sûr le numéro de la partition. Voici ce que cela donne:

commande : _*"select partition"*_

```La partition 1 est maintenant la partition sélectionnée.```

commande : _*"detail partition"*_

```
Partition 1
Type    : c12a7328-f81f-11d2-ba4b-00a0c93ec93b
Masqué  : Oui
Requis  : Non
Attrib  : 0X8000000000000000
Décalage en octets : 1048576

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 2         SYSTEM       FAT32  Partition    100 M   Sain       Système
```


**Expliquer la fonction de chaque partition :**


J'utilise la commande _*"Get-Partition"*_ pour pouvoir afficher toute les informations dont j'ai besoin.

La première ligne "System" indique l'endroit de notre os sur notre pc

La partie "Reserved" est une partie réservée à la mémoire ram

La partie "Basic" est le lieu de stockage des données utilisateur

Enfin la partie "Recovery" est une partition de secours, elle est présente au cas où le pc est un problème de fonctionnement

```
   DiskPath : \\?\scsi#disk&ven_nvme&prod_samsung_mzvlb256#5&316814fb&0&000000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

PartitionNumber  DriveLetter Offset                                        Size Type
---------------  ----------- ------                                        ---- ----
1                            1048576                                     100 MB System
2                            105906176                                    16 MB Reserved
3                C           122683392                                    80 GB Basic
4                D           86022029312                              142.86 GB Basic
5                            239417163776                                512 MB Recovery
6                            239954034688                                 14 GB Recovery
7                            254986420224                                  1 GB Recovery

```
---

# **USERS**

🌞 Déterminer la liste des utilisateurs de la machine

**La liste complète des utilisateurs de la machine :** Pour ce faire, j'utilise la commande _*"wmic useraccount list full"*_ qui donne la liste des utilisateurs de la machine :

```
AccountType=512
Description=Compte d'utilisateur d'administration
Disabled=TRUE
Domain=LAPTOP-Q13R3KDP
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Administrateur
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-3805659845-2083363258-1453832755-500
SIDType=1
Status=Degraded


AccountType=512
Description=
Disabled=FALSE
Domain=LAPTOP-Q13R3KDP
FullName=Tsuyo CY
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=anton
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-3805659845-2083363258-1453832755-1001
SIDType=1
Status=OK


AccountType=512
Description=Compte utilisateur géré par le système.
Disabled=TRUE
Domain=LAPTOP-Q13R3KDP
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=DefaultAccount
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-3805659845-2083363258-1453832755-503
SIDType=1
Status=Degraded


AccountType=512
Description=
Disabled=FALSE
Domain=LAPTOP-Q13R3KDP
FullName=New User
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=defaultuser100001
PasswordChangeable=FALSE
PasswordExpires=TRUE
PasswordRequired=TRUE
SID=S-1-5-21-3805659845-2083363258-1453832755-1003
SIDType=1
Status=OK


AccountType=512
Description=Compte d'utilisateur invité
Disabled=TRUE
Domain=LAPTOP-Q13R3KDP
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Invité
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-3805659845-2083363258-1453832755-501
SIDType=1
Status=Degraded


AccountType=512
Description=Compte d'utilisateur géré et utilisé par le système pour les scénarios Windows Defender Application Guard.
Disabled=TRUE
Domain=LAPTOP-Q13R3KDP
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=WDAGUtilityAccount
PasswordChangeable=TRUE
PasswordExpires=TRUE
PasswordRequired=TRUE
SID=S-1-5-21-3805659845-2083363258-1453832755-504
SIDType=1
Status=Degraded

```

**Déterminer le nom de l'utilisateur qui est full admin sur la machine :**

* il existe toujours un utilisateur particulier qui a le droit de tout faire sur la machine

* pour les Windowsiens : faites des recherches sur NT-AUTHORITY\SYSTEM et le concept de SID

J'utilise la commande _*"net user"*_ pour savoir quel est le nom de l'utilisateur qui est full admin sur la machine : 

```
comptes d’utilisateurs de \\LAPTOP-Q13R3KDP

-------------------------------------------------------------------------------
Administrateur           anton                    DefaultAccount
Invité                   WDAGUtilityAccount
La commande s’est terminée correctement.
```

---

# **PROCESSUS**

🌞 Déterminer la liste des processus de la machine

**Choisissez 5 services système et expliquer leur utilité :**

1. Interface graphique : Une interface graphique est un endroit qui nous permet de dialoguer directement avec notre machine, ou permet à plusieurs logiciels de dialoguer entre eux.

2. Démon réseau : C'est un type de programme, un processus ou bien un ensemble de processus qui va s'exécuter non pas directement par l'utilisateur mais en arrière-plan. Ils servent généralement à répondre à des requêtes du réseau, à l'activité du matériel ou à d'autres programmes en exécutant certaines tâches

3. Taskhostw.exe : Le "Taskhostw.exe" est un processus hôte pour les tâches Windows. C'est un fichier servant d’hôte aux processus basés sur les DLL. Dans le gestionnaire de tâches, ces processus sont affichés avec le nom Processus hôte pour les tâches Windows.

4. Wininit.exe: Il est responsable de l'exécution du processus d'initialisation Windows

5. ??


**Déterminer les processus lancés par l'utilisateur qui est full admin sur la machine :**



---

# **NETWORK**

🌞 Afficher la liste des cartes réseau de votre machine

J'affiche la liste de cartes réseaux de ma machine à l'aide la commande _*"ipconfig"*_ :

```
Configuration IP de Windows


Carte Ethernet VirtualBox Host-Only Network :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::342d:d370:b953:fdcf%3
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.56.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . :

Carte réseau sans fil Connexion au réseau local* 1 :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :

Carte réseau sans fil Connexion au réseau local* 2 :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::b1da:c56d:bcf4:d995%14
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.54
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . : 192.168.1.1

Carte Ethernet Connexion réseau Bluetooth :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :
```

**Expliquer la fonction de chacune d'entre elles :**

* exemple : carte WiFi, carte de loopback, etc.

La Carte Ethernet VirtualBox Host-Only Network : Le mode "Host-Only" permet à mon ordinateur de discuter avec ma VM, mais ma VM ne peux pas aller sur Internet.

La Carte réseau sans fil Connexion au réseau local : Une carte réseau sans fil est un périphérique permettant de connecter mon ordinateur à un réseau sans fil.

La Carte réseau sans-fil Wi-Fi : C'est une norme de communication permettant la transmission de données numériques sans fil. Elle permet de préparer , d'envoyer et de contrôler les données sur le réseau. Une carte wifi est une d'interface physique entre l'ordinateur et le modem




🌞 Lister tous les ports TCP et UDP en utilisation

Je peux afficher tout les ports TCP et UDP en utilisation à partir de la commande _*"netstat -a"*_ :
```
Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            LAPTOP-Q13R3KDP:0      LISTENING
  TCP    0.0.0.0:445            LAPTOP-Q13R3KDP:0      LISTENING
  TCP    0.0.0.0:5040           LAPTOP-Q13R3KDP:0      LISTENING
  TCP    0.0.0.0:49664          LAPTOP-Q13R3KDP:0      LISTENING
  TCP    0.0.0.0:49665          LAPTOP-Q13R3KDP:0      LISTENING
  TCP    0.0.0.0:49666          LAPTOP-Q13R3KDP:0      LISTENING
  TCP    0.0.0.0:49667          LAPTOP-Q13R3KDP:0      LISTENING
  TCP    0.0.0.0:49668          LAPTOP-Q13R3KDP:0      LISTENING
  TCP    0.0.0.0:49669          LAPTOP-Q13R3KDP:0      LISTENING
  TCP    127.0.0.1:27060        LAPTOP-Q13R3KDP:0      LISTENING
  TCP    192.168.1.54:139       LAPTOP-Q13R3KDP:0      LISTENING
  TCP    192.168.1.54:50683     52.97.150.2:https      ESTABLISHED
  TCP    192.168.1.54:50924     52.114.92.91:https     ESTABLISHED
  TCP    192.168.1.54:50925     40.67.251.132:https    ESTABLISHED
  TCP    192.168.1.54:50926     Google-Home:8009       ESTABLISHED
  TCP    192.168.1.54:50948     Google-Home:32085      ESTABLISHED
  TCP    192.168.1.54:50949     wa-in-f188:5228        ESTABLISHED
  TCP    192.168.1.54:50954     52.113.199.5:https     ESTABLISHED
  TCP    192.168.1.54:50971     155.133.248.39:27037   ESTABLISHED
  TCP    192.168.1.54:51281     par21s11-in-f10:https  CLOSE_WAIT
  TCP    192.168.1.54:52004     ec2-18-179-225-220:https  CLOSE_WAIT
  TCP    192.168.1.54:52006     152.199.21.187:https   ESTABLISHED
  TCP    192.168.1.54:52007     152.195.34.118:https   ESTABLISHED
  TCP    192.168.1.54:52009     52.114.75.54:https     ESTABLISHED
  TCP    192.168.1.54:52010     reflectededge:https    ESTABLISHED
  TCP    192.168.1.54:52011     vip142:https           CLOSE_WAIT
  TCP    192.168.1.54:52013     vip142:https           CLOSE_WAIT
  TCP    192.168.1.54:52014     vip142:https           CLOSE_WAIT
  TCP    192.168.1.54:52016     vip142:https           CLOSE_WAIT
  TCP    192.168.1.54:52017     reflectededge:https    ESTABLISHED
  TCP    192.168.1.54:52019     reflectededge:https    ESTABLISHED
  TCP    192.168.1.54:52020     vip085:https           CLOSE_WAIT
  TCP    192.168.1.54:52021     216.18.168.166:https   CLOSE_WAIT
  TCP    192.168.1.54:52022     reflectededge:https    ESTABLISHED
  TCP    192.168.1.54:52025     192.229.221.215:https  ESTABLISHED
  TCP    192.168.1.54:52026     192.229.221.215:https  ESTABLISHED
  TCP    192.168.1.54:52027     40.90.137.125:https    ESTABLISHED
  TCP    192.168.56.1:139       LAPTOP-Q13R3KDP:0      LISTENING
  TCP    [::]:135               LAPTOP-Q13R3KDP:0      LISTENING
  TCP    [::]:445               LAPTOP-Q13R3KDP:0      LISTENING
  TCP    [::]:49664             LAPTOP-Q13R3KDP:0      LISTENING
  TCP    [::]:49665             LAPTOP-Q13R3KDP:0      LISTENING
  TCP    [::]:49666             LAPTOP-Q13R3KDP:0      LISTENING
  TCP    [::]:49667             LAPTOP-Q13R3KDP:0      LISTENING
  TCP    [::]:49668             LAPTOP-Q13R3KDP:0      LISTENING
  TCP    [::]:49669             LAPTOP-Q13R3KDP:0      LISTENING
  UDP    0.0.0.0:5050           *:*
  UDP    0.0.0.0:5353           *:*
  UDP    0.0.0.0:5353           *:*
  UDP    0.0.0.0:5353           *:*
  UDP    0.0.0.0:5353           *:*
  UDP    0.0.0.0:5353           *:*
  UDP    0.0.0.0:5353           *:*
  UDP    0.0.0.0:5353           *:*
  UDP    0.0.0.0:5353           *:*
  UDP    0.0.0.0:5353           *:*
  UDP    0.0.0.0:5355           *:*
  UDP    0.0.0.0:27036          *:*
  UDP    0.0.0.0:49703          *:*
  UDP    0.0.0.0:51604          *:*
  UDP    0.0.0.0:52593          *:*
  UDP    0.0.0.0:56444          *:*
  UDP    0.0.0.0:57129          *:*
  UDP    0.0.0.0:59137          *:*
  UDP    0.0.0.0:59138          *:*
  UDP    0.0.0.0:59139          *:*
  UDP    127.0.0.1:1900         *:*
  UDP    127.0.0.1:62218        *:*
  UDP    127.0.0.1:63955        *:*
  UDP    192.168.1.54:137       *:*
  UDP    192.168.1.54:138       *:*
  UDP    192.168.1.54:1900      *:*
  UDP    192.168.1.54:63954     *:*
  UDP    192.168.56.1:137       *:*
  UDP    192.168.56.1:138       *:*
  UDP    192.168.56.1:1900      *:*
  UDP    192.168.56.1:63953     *:*
  UDP    [::]:5353              *:*
  UDP    [::]:5353              *:*
  UDP    [::]:5353              *:*
  UDP    [::]:5353              *:*
  UDP    [::]:5353              *:*
  UDP    [::]:5355              *:*
  UDP    [::]:51604             *:*
  UDP    [::]:52593             *:*
  UDP    [::1]:1900             *:*
  UDP    [::1]:63952            *:*
  UDP    [fe80::342d:d370:b953:fdcf%3]:1900  *:*
  UDP    [fe80::342d:d370:b953:fdcf%3]:63950  *:*
  UDP    [fe80::b1da:c56d:bcf4:d995%14]:1900  *:*
  UDP    [fe80::b1da:c56d:bcf4:d995%14]:63951  *:*
```

**Déterminer quel programme tourne derrière chacun des ports :**


**Expliquer la fonction de chacun de ces programmes :**

---

# **II.SCRIPTING**

Vous devrez :

* commenter votre script

* mettre un en-tête à votre script

shebang si besoin (GNU/Linux et MacOS)
auteur
date
description succincte de ce que fait le script


* éviter le plus possible que le script ait besoin de se lancer avec des privilèges élevés

on respecte le principe du moindre privilège

sous GNU/Linux vous n'avez normalement pas besoin de root pour ce script


🌞 Utiliser un langage de scripting natif à votre OS

* trouvez un langage natif par rapport à votre OS
* l'utiliser pour coder un script qui

1. affiche un résumé de l'OS

nom machine
IP principale
OS et version de l'OS
date et heure d'allumage
détermine si l'OS est à jour
Espace RAM utilisé / Espace RAM dispo
Espace disque utilisé / Espace disque dispo


2. liste les utilisateurs de la machine
3. calcule et affiche le temps de réponse moyen vers 8.8.8.8

avec des ping

```
Nom de l'ordinateur : LAPTOP-Q13R3KDP
Os : Microsoft Windows 10 Famille
Os Version : 10.0.18363
Date et heure d'allumage : 10/18/2020 12:55:58
Le systeme est t'il a jour : Non

Ram totale : 6.9371452331543 Go
RAM disponible : 0.650730133056641 Go
Ram utiliser : 6.28641510009766 Go
Espace disque utiliser : 8 Go
Espace disque disponible : 72 Go

Ip principale : 192.168.56.1
 - Ping : 49.75 ms
 - download speed : 0 Mbit/s
 - upload speed : 0 Mbit/s

```
🌞 Créer un deuxième script qui permet, en fonction d'arguments qui lui sont passés :

* exécuter une action

* lock l'écran après X secondes
pour lock mon pc, j'utilise le nom de mon script avec ensuite "lock" puis le nombre de secondes avant que mon pc se lock. Si je veux lock mon pc 1à secondes, je note la commande suivante : *_"TP1.ps1 lock 10"_*

* éteindre le PC après X secondes

Même principe que pour le lock, si je veux éteindre mon pc au but de 1à secondes par exemple, j'exécute la commande suivante : _*"TP2.ps1 shutdown 10"*_

```
#Script qui execute des action entré en argument 
#Auteur : Jouannet Antonin
#Date : 31/10/2020

#Le if permet de verouiller l'ecran au bout d'un temps que l'on entre en argument.
if ($args[0] -match "lock") {
    Start-Sleep -Seconds $args[1]                
    rundll32.exe user32.dll, LockWorkStation
}
#Le elseif sert a eteindre l'ordinateur au bout d'un temps que l'on entre en argument.
elseif ($args -match "shutdown") {
    shutdown /s /t $args[1]                    
}
#Le else permet de dire que l'argument entré est bon ou pas.
else {
    echo "wrong arguments"                      
}
```


exemple d'utilisation :
---

# **III.GESTION DE SOFT**

🌞 Expliquer l'intérêt de l'utilisation d'un gestionnaire de paquets

* par rapport au téléchargement en direct sur internet

* penser à l'identité des gens impliqués dans un téléchargement (vous, l'éditeur logiciel, etc.)

* penser à la sécurité globale impliquée lors d'un téléchargement

Un gestionnaire de paquets est un outil permettant d'automatiser le processus d'installation, désinstallation et mise à jour de logiciels installés sur un système informatique. Ils permettent de mettre à disposition simplement des milliers de paquetages lors d'une installation standard.
Pour bien comprendre, un paquet est une archive qui comprend les fichiers informatiques, les informations et procédures nécessaires à l'installation d'un logiciel sur un système d'exploitation au sein d'un agrégat logiciel (Ensemble de contenus logiciels, ayant chacun une spécificité qui se complètent entre eux et qui ne peuvent fonctionner que réunis.), tout en s'assurant de la cohérence fonctionnelle du système ainsi modifié.

Pour donner des exemples, pour Linux ou macOS on pourrait citer comme gestionnaire de paquets : Pacman, Homebrew
Pour Windows : Chocolatey, Package Manager.

En ce qui concerne la sécurité, on peut par exemple configurer automatiquement les versions de mises à jour de sécurité des paquets et demander une autorisation du développeur pour les mises à jour les plus importantes.



🌞 Utiliser un gestionnaire de paquet propres à votre OS pour:

* lister tous les paquets déjà installés
 
* déterminer la provenance des paquets (= quel serveur nous délivre les paquets lorsqu'on installe quelque chose)

---

# **IV.PARTAGE DE FICHIERS**

Créer le serveur (sur votre PC)

Créer un partage de fichiers SUR VOTRE PC (pas dans la VM) :

* si vous avez Windows, ce sera avec Samba

un simple clic-droit > propriétés


* sous GNU/Linux je vous conseille de faire un partage NFS (ou Samba)
sous MacOS, vous pouvez aussi faire un partage NFS


ACCEDER AU PARTAGE DE LA VM

Accéder au partage de fichiers depuis la machine virtuelle

* le dossier partagé doit être accessible dans la VM
* vous devez donc pouvoir créer, éditer, et supprimer des fichiers depuis la machine virtuelle
* vous devez prouver que tout ceci fonctionne


Pour installer Samba, je vais dans mon panneau de configuration puis je rentre dans Programmes/Programmes et fonctionnalités, puis sélectionne _**"Activer ou désactiver des fonctionnalités Windows"**_. Enfin, je rentre dans le dossier _**"Support de partage de fichiers SMB 1.0/CIFS"**_ et je coche les deux premières cases.
J'ouvre mon PowerShell et exécute la commande _"yum install -y cifs-utils"_ pour installer Samba.


Voici comment établir une connexion ssh depuis PowerShell :
```
PS C:\Windows\system32> ssh root@192.168.120.50
root@192.168.120.50's password:
Last login: Mon Nov  2 19:23:41 2020 from 192.168.120.1
```

Je créé un dossier pour effectuer des partages entre la VM et mon pc.
```
PS C:\Windows\system32> New-SmbShare -Name Share -Path "C:\Share" -FullAccess "Tout le monde"

Name  ScopeName Path     Description
----  --------- ----     -----------
Share *         C:\Share
```


Je créé ensuite un dossier avec la commande _"mkdir /opt/partage"_ et monte le partage dans la VM avec la commande _"mount -t cifs -o username=<VOTRE_UTILISATEUR>,password=<VOTRE_MOT_DE_PASSE> //<IP_DE_VOTRE_PC>/<NOM_DU_PARTAGE> /opt/partage"_ en remplaçant les éléments demandés :
```
[root@localhost ~]# mkdir /opt/partage
[root@localhost ~]# mount -t cifs -o username=Tsuyo CY,password=101218//192.168.1.54/Share /opt/partage
mount.cifs: bad UNC (CY,password=101218//192.168.1.54/Share)
```

Je me place dans mon dossier partage et créé un fichier text.txt puis l'affiche avec ls :
```
[root@localhost ~]# cd /opt/partage/
[root@localhost partage]# touch text.txt
[root@localhost partage]# ls
text.txt
```

On retourne sur notre Powershell et si je rentre dans mon dossier Share, le fichier text.txt devrait apparaître:
```
Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----        14/11/2020     14:20              0 text.txt
```
